// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 12, 2021
// This file is the implementation of Pgm class found in include/pgm.hpp. It contains all of the methods used to read,
// write, create, rotate, panel and crop PGM files. These are the functions that will be called in src/pgm_tester.

#include "pgm.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

// this method reads a PGM from a file into the Pixels vector. it was included in Lab writeup
bool Pgm::Read(const std::string &file) {
	ifstream fin;
  	string s;
  	size_t i, j, r, c, v;

  	fin.open(file.c_str());
  	if (fin.fail()) return false;

  	if (!(fin >> s)) return false;
  	if (s != "P2") return false;
  	if (!(fin >> c) || c <= 0) return false;
  	if (!(fin >> r) || r <= 0) return false;
  	if (!(fin >> i) || i != 255) return false;

  	Pixels.resize(r);
  	for (i = 0; i < r; i++) {

		Pixels[i].clear();
		for (j = 0; j < c; j++) {

			if (!(fin >> v) || v > 255) return false;
			Pixels[i].push_back(v);

    		}
  	}

  	if (fin >> s) return false;

  	fin.close();

  	return true;
}

// this method writes a file from the Pixels vector
bool Pgm::Write(const string &file) const {
	ofstream fout;	// to output to file
	size_t r, c, v; // info from Pixels
	size_t i, j, k;	// iterators

	// fout opens file name
	fout.open(file.c_str());
	if(fout.fail())	return false;

	// if theres nothing to write, return false
	if(Pixels.empty()) return false;

	// r and c set to be equal to to rows and cols
	r = Pixels.size();
	c = Pixels[0].size();

	// outputs the first three lines of PGM file (the header, if you will)
	fout << "P2\n" << c << " " << r << "\n" << 255;

	// iterates thru the pixels array
	k = 0;
	for(i = 0; i < Pixels.size(); i++){
		for(j = 0; j < Pixels[i].size(); j++){
			// if 20 pixels have been output, then a new line is added and count restarts
			if(k%20 == 0) {
				fout << endl;
				k = 0;
			}  else fout << " ";
			// other wise, a space is output

			//outputs the xurrent pixel value
			v = Pixels[i].at(j);
			fout << v;

			// increments pixel output count
			k++;

		}
	}

	// new line at end of array
	fout << endl;

	// closes file and returns
	fout.close();

	return true;
}

// creates a PGM file of rows r, cols c, filled with pixels of value pv
bool Pgm::Create(size_t r, size_t c, size_t pv){
	size_t i, j;	// iterators

	if(pv > 255) return false; // if pv is not between 0 and 255, return false.

	Pixels.resize(r); // resize pixels to be size r

	for(i = 0; i < r; i++){

		Pixels[i].clear(); // clear this row of pixels before pushing back

		// push back j elements of value pv
		for(j = 0; j < c; j++) Pixels[i].push_back(pv);

	}

	return true;
}

// this method rotates the image clockwise
bool Pgm::Clockwise(){
	vector < vector <int> > temp;	// temp will become rotated image
	size_t i, j;			// iterators
	size_t origR, origC;		// original rows and columns, to clean up code

	// sets origR and orgiC to be rows and cols of PIxels
	origR = Pixels.size();
	origC = Pixels[0].size();

	// temp is resized to be origC rows
	temp.resize(origC);

	// this clones Pixels into temp but rotated 90 deg cw
	for(i = 0; i < origC; i++){
		temp[i].clear();
		for(j = 0; j < origR; j++){
			temp[i].push_back(Pixels[origR- j - 1].at(i));
		}
	}

	// Pixels is cleared and set equal to temp
	Pixels.clear();
	Pixels = temp;


	return true;
}

// this method rotates the image counter clockwise
// it is identical to Clockwise() except for the indexing of Pixels during push_back
bool Pgm::Cclockwise(){
	vector < vector <int> > temp;	// temp will become rotated image
	size_t i, j;			//iterators
	size_t origR, origC;		// original rows and columns, to clean up code

	// if pixels is empty, return false
	if(Pixels.empty()) return false;

	// sets origR and origC to be the rows and cols of PIxels
	origR = Pixels.size();
	origC = Pixels[0].size();

	// resizes temp to be OrigC rows
	temp.resize(origC);

	// this loop clones Pixels rotated 90 deg ccw
	for(i = 0; i < origC; i++){
		temp[i].clear();
		for(j = 0; j < origR; j++){
			temp[i].push_back(Pixels[j].at(origC - i - 1));
		}
	}

	// Pixels is cleared and set equal to temp
	Pixels.clear();
	Pixels = temp;


	return true;
}

// this method adds a border of width w filled with pixel value pv around the image
bool Pgm::Pad(size_t w, size_t pv){
	vector < vector <int> > temp;	// temp will become image with padding
	size_t i, j, k;			// iterators

	// if pv is to high for PGM, return false
	if(pv > 255) return false;
	// if Pixels is empty, return false
	if(Pixels.empty()) return false;

	temp.resize(Pixels.size() + (2 * w));
	for(i = 0; i < Pixels.size() + (2 * w); i++){
		temp[i].clear();

		// adds a width of pv at left of picture
		for(k = 0; k < w; k++) temp[i].push_back(pv);

		for(j = 0; j < Pixels[0].size(); j++){

			// if i is in the space of the border, push back pv
			if(i < w || i >= temp.size() - w) temp[i].push_back(pv);
			else temp[i].push_back(Pixels[i-w].at(j));
			// else, push back the pixels from PIxels

		}

		// and again, another band of pv
		for (k = 0; k < w; k++) temp[i].push_back(pv);
	}

	// Pixels is cleared and set equal to temp
	Pixels.clear();
	Pixels = temp;

 	return true;
 }

// this method creates a tessellation of r*c panels of the image
bool Pgm::Panel(size_t r, size_t c){
	vector < vector <int> > temp;	// temp will become the paneled image
	size_t h, i, j, k;		// iterators

	// if Pixels is empty, return false
	if(Pixels.empty()) return false;
	// if rows or colums to tile is 0, return false
	if(r == 0 || c == 0) return false;

	// resizes temp to be size of rows*r rows
	temp.resize(Pixels.size() * r);

	// makes r copies of the tesselated row of pictures
	for(h = 0; h < r; h++){
		// makes all the lines of the picture
		for(i = 0; i < Pixels.size(); i++){
			temp[i + (h * Pixels.size())].clear();

			// makes c copies of each line in the picutre
			for(j = 0; j < c; j++){
				// each line from the pixel
				for(k = 0; k < Pixels[0].size(); k++){
					temp[i + (h*Pixels.size())].push_back(Pixels[i].at(k));
				}
			}
		}
	}

	// clears Pixels and sets it eqaul to temp
	Pixels.clear();
	Pixels = temp;

	return true;
}

// this method crops the image so the top left is at (r,c) and bottom left is at (r+rows, c+cols)
bool Pgm::Crop(size_t r, size_t c,  size_t rows, size_t cols) {
	vector < vector <int> > temp;	// temp will become the cropped image
	size_t i, j;			// iterators

	// if the crop requested will exit the bounds of the image, return false
	if(r+rows > Pixels.size() || c+cols > Pixels[0].size()) return false;

	if(Pixels.empty()) return false;

	// clones elements from the cropped area in Pixels into temp
	temp.resize(rows);
	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			temp[i].push_back(Pixels[i+r].at(j+c));
		}
	}

	// clears Pixels and sets it eqaul to temp
	Pixels.clear();
	Pixels = temp;

	return true;
}
