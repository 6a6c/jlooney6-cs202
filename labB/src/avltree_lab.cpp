// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; Decemeber 03, 2021
// This file contains the Insert, Delete, Height, and Ordered_keys for an AVL tree. It implements the AVLTree class found in
// include/avltree.hpp and it is utilizied by src/avltree_tester.

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include "avltree.hpp"
using namespace std;
using CS202::AVLTree;
using CS202::AVLNode;

// Assignment overload
AVLTree& AVLTree::operator= (const AVLTree &t) {
    AVLNode* root;                      // the new root for this

    this->Clear();                      // clears this

    // Usess recursive_post_order_copy to clone tree t
    root = this->recursive_postorder_copy(t.sentinel->right);
    root->parent = sentinel;            // Sets root's parent to sentinel
    sentinel->right = root;             // And sentinel's child to root

    this->size = t.size;                // Updates size

    return *this;
}

// Imbalance returns true if there is imbalance about node n
bool imbalance(AVLNode *n){
   int leftHeight, rightHeight;         // For cleanliness
   leftHeight = n->left->height;
   rightHeight = n->right->height;

   if(n->height == 0) return false;     // Sentinel cant be imbalanced

   // returns true if difference between two children's heights is > 1. false otherwise
   if(abs(leftHeight - rightHeight) > 1)  return true;
   else return false;
}

// Fix height is an idempotent process that calulates node n's height based on it's children
void fix_height(AVLNode *n){
    size_t childHeight;                 // The max height of the two children

    if( n->height == 0 ) return;        // Don't try and update sentinel's height!

    // Sets childHeight to max of n's two children
    childHeight = (n->left->height < n->right->height) ? n->right->height : n->left->height;

    // Sets n's height to childHeight+1 (one above child)
    n->height = childHeight + 1;
}

// Rotate rotates about a given node n
void rotate(AVLNode *n){
    AVLNode *parent, *grandparent, *middle, *child;     //Nodes to keep track of

    // Sets all nodes based on relation to node n
    parent = n->parent;
    grandparent = parent->parent;
    middle = (parent->right == n) ? parent->left : parent->right;
    child = (parent->right == n) ? n->left : n->right;

    if( parent->height == 0 ) return;                   // Don't rotate if parent is sentinel

    // Rotation starts by swapping parent with n
    if( grandparent->right == parent) grandparent->right = n;
    else grandparent->left = n;
    n->parent = grandparent;

    // Then moves child and middle based on parent's relation to n
    if( parent->right == n ){
        parent->right = child;
        if(child->height != 0) child->parent = parent;

        parent->left = middle;
        if(middle->height != 0) middle->parent = parent;

        n->left = parent;
        parent->parent = n;
    } else {
        parent->left = child;
        if(child->height != 0) child->parent = parent;

        parent->right = middle;
        if(middle->height != 0)middle->parent = parent;

        n->right = parent;
        parent->parent = n;
    }

    // Finally, updates the heights of nodes that moved
    fix_height(parent);
    fix_height(n);
    fix_height(middle);

}

// Fix imbalance will determine if an imbalance is a zig zig or zig zag, and call rotate accordingly.
void fix_imbalance(AVLNode *n){
    AVLNode *left, *right, *grandchild;

    left = n->left;
    right = n->right;

    // Deteremines if imbalance needs left rotation or right rotation
    if(right->height > left->height){

        // If right->right >=, than it is a zig zig. Otherwise, it's a zig-zag
        // (The ">=" is very imporant for delete, and caused me 4 hours of bugs)
        if(right->right->height >= right->left->height){
            // ZIG_ZIG
            rotate(right);
        } else{
            // ZIG_ZAG
            grandchild = right->left;
            rotate(grandchild);
            rotate(grandchild);
        }

    }else{

        // Similar to above, just mirrored
        if(left->left->height >= left->right->height){
            // ZIG_ZIG
            rotate(left);
        } else if(left->left->height < left->right->height){
            // ZIG_ZAG
            grandchild = left->right;
            rotate(grandchild);
            rotate(grandchild);
        }

    }

}

// Insert inserts a node ala a regular BSTree, but then iterates up through tree fixing heights and rebalancing.
bool AVLTree::Insert(const string &key, void *val) {
    AVLNode *parent;
    AVLNode *n;
    AVLNode *nit;       // nit is used to iterate up throguh tree

    parent = sentinel;
    n = sentinel->right;

    /* Find where the key should go.  If you find the key, return false. */

    while (n != sentinel) {
        if (n->key == key) return false;
        parent = n;
        n = (key < n->key) ? n->left : n->right;
    }

    /* At this point, parent is the node that will be the parent of the new node.
        Create the new node, and hook it in. */

    n = new AVLNode;
    n->key = key;
    n->val = val;
    n->parent = parent;
    n->height = 1;
    n->left = sentinel;
    n->right = sentinel;

    /* Use the correct pointer in the parent to point to the new node. */

    if (parent == sentinel) {
        sentinel->right = n;
    } else if (key < parent->key) {
        parent->left = n;
    } else {
        parent->right = n;
    }

    /* Increment the size */
    size++;

    // ^^All code above is ripped straight from BSTree implementation

    // Fix heights and imbalances by iterating up thru tree
    nit = n;
    while(nit!= sentinel){                      // Starts at bottom and ends at root
        fix_height(nit);                        //      calls fix height on nit
        if(imbalance(nit)){                     //      if theres and imbalance about nit:
            fix_imbalance(nit);                 //              fixes imbalance
            break;                              //              then, tree is balanced and correct. breaks
        }                                       //
        nit = nit->parent;                      //      otherwise, continues up tree
    }

    return true;
}

// Delete is like insert: deletes a node the same was a BSTree, but then iterates up fixing heights and rebalancing
bool AVLTree::Delete(const string &key) {
    AVLNode *n, *parent, *mlc;
    string tmpkey;
    void *tmpval;
    AVLNode *nit;       // nit is used to iterate up through tree

    /* Try to find the key -- if you can't return false. */

    n = sentinel->right;
    while (n != sentinel && key != n->key) {
        n = (key < n->key) ? n->left : n->right;
    }
    if (n == sentinel) return false;

    /* We go through the three cases for deletion, although it's a little
     different from the canonical explanation. */

    parent = n->parent;

    if (n->left == sentinel) {

        if (n == parent->left) {
            parent->left = n->right;
        } else {
            parent->right = n->right;
        }
        if (n->right != sentinel) n->right->parent = parent;
        delete n;
        size--;

    } else if (n->right == sentinel) {

        if (n == parent->left) {
            parent->left = n->left;
        } else {
            parent->right = n->left;
        }
        if(n->left != sentinel) n->left->parent = parent;
        delete n;
        size--;

    } else {

        for (mlc = n->left; mlc->right != sentinel; mlc = mlc->right) ;
        tmpkey = mlc->key;
        tmpval = mlc->val;
        Delete(tmpkey);
        n->key = tmpkey;
        n->val = tmpval;
        return true;
    }

    // ^^ Above code is ripped straight from BSTree implementaion

    // Iterates up through tree fixing heights and rebalancing
    nit = parent;       // As n is deleted, we start with parent
    while(nit != sentinel){
        fix_height(nit);
        if(imbalance(nit)){
            fix_imbalance(nit);
            fix_height(nit);
        }
        nit = nit->parent;
    }

    return true;
}

// Ordered keys is ripped straight from BSTree implementation
vector <string> AVLTree::Ordered_Keys() const{
   vector <string> rv;

    make_key_vector(sentinel->right, rv);       // Calls make_key_vector on root of the tree

    return rv;

}

// make_key_vector is ripped straight from BSTree implementation
void AVLTree::make_key_vector(const AVLNode *n, vector<string> &v) const{
    if( n->left != sentinel ) make_key_vector(n->left, v);
    v.push_back(n->key);
    if( n->right != sentinel ) make_key_vector(n->right, v);
}

// Height returns the height of the root
size_t AVLTree::Height() const {
    return sentinel->right->height;
}

// Copies the tree rooted by node n. It is similar to the ordered copy from the BSTree lab
AVLNode *AVLTree::recursive_postorder_copy(const AVLNode *n) const {
    AVLNode* ret;

    // Checks in n is sentinel with height
    if(n->height == 0) return sentinel;

    // Assigns values of ret to be copy of n
    ret = new AVLNode;
    ret->key = n->key;
    ret->val = n->val;
    ret->height = n->height;

    // Sets left to copy of n's left
    ret->left = recursive_postorder_copy(n->left);
    if(ret->left != sentinel) ret->left->parent = ret;

    // Sets right to copy of n's right
    ret->right = recursive_postorder_copy(n->right);
    if(ret->right != sentinel) ret->right->parent = ret;

    // retruns ret, post order
    return ret;
}
