#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

int main(int argc, char* argv[]){
	// you can also use char** argv apparently

	vector <string> sv(10);
	string s, w;
	int i, nl;
	istringstream ss;

	/* Output the last 10 lines of strings read in from standard in
	nl = 0;
	while (getline(cin, s)){
		sv[nl%10] = s;
		nl++;
	}

	i = nl -10;
	if (i < 0) i = 0;

	for( ; i < nl; i++){
		cout << sv[i%10] << endl;
	}
	*/

	/*
	 * Territary elements:
	 * hell if i know dude
	for (i = (sv.size() < 10) ? 0 : sv.size()-10; i < (int) sv.size(); i++){
		cout << sv[i] << endl;
	}
	*/

	for( i = 0; i < argc; i++){
		printf("%3d %s\n", i, argv[i]);
	}

	// These two strings will never equal each other, because they are C-Style strings!
	// It is a comparisson of pointers to the C-Style strings, and they are two different points
	// in memory! So, always convert to C++ style strings and then compare/alter the strings.
	if(argv[1] == argv[2]) printf("They equal each other!\n");
	else printf("They do not!\n");

	// This shit will only work for the argv[1] because string streams suck and were created by the
	// devil in order to tempt the pure into sin.
	ss.str(argv[1]);

	if(ss >> i) printf("Integer! %d", i);
	else printf("Not an integer!\n");

	// A good use of string streams to tho is to read in individual words from a getline like below:
	while(getline(cin,s)){
		sv.clear();
		ss.clear();
		ss.str(s);
		while (ss >> w) sv.push_back(w);

		printf("# Words; %lu. Last word: %s\n", sv.size(), sv.at(sv.size()-1).c_str());
		// Vectors are cool cuz they will output null for vector size 0
	}


	// Vandermann matrix: always invertible with invertible sub matrices
	vector < vector <int> > vdm;



	return 0;
}
