// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; October 06, 2021
// This file implements the fraction class defined in include/fraction.hpp. It creates and edits a fraction based on how you would on paper.
// Multiplying adds elements to the top, dividing adds elements to the bottom. If a number is in both the numerator and the denomiator,
// it cancels out.

#include "fraction.hpp"
#include <iostream>
using namespace std;

// clears multisets
void Fraction::Clear(){
	numerator.clear();
	denominator.clear();
}

// "Multiplies" by adding number to numerator/removing from denominator
bool Fraction::Multiply_Number(int n){
	// returns false in n is non-positive
	if(n <= 0) return false;

	// if n is in the denominator, we remove it as multiplying
	// by n in numerator will cancel out n in denominator
	if(denominator.find(n) != denominator.end()){
		denominator.erase(denominator.find(n));
	} else {
		numerator.insert(n);
	}

	return true;
}

// "Divides" by by adding number to numerator/removing frmo denominator
bool Fraction::Divide_Number(int n){
	// returns false if n is non-positive
	if(n <= 0) return false;

	// if n is in the numerator, we remove it as multiplying
	// by n in numerator will cancel out n in denominator
	if(numerator.find(n) != numerator.end()){
		numerator.erase(numerator.find(n));
	} else {
		denominator.insert(n);
	}

	return true;
}

// "Multiplies" but with all numbers between n and 2
bool Fraction::Multiply_Factorial(int n){
	size_t i;	// iterator

	// returns false if n is non positive
	if(n <= 0) return false;

	// inserts or cancels for all numers in n!
	for(i = n; i > 1; i--){
		// cancels if i is in denom, otherwise adds to  numer
		if(denominator.find(i) != denominator.end()){
			denominator.erase(denominator.find(i));
		} else {
			numerator.insert(i);
		}

	}

	return true;
}

// "Divides" but with all numbers between n and 2
bool Fraction::Divide_Factorial(int n){
	size_t i;	// iterator

	// returns false if n is non positive
	if(n <= 0) return false;

	// inserts or cancels for all numers in n!
	for(i = n; i > 1; i--){
		// cancels if i is in numer, otherwise adds to denom
		if(numerator.find(i) != numerator.end()){
			numerator.erase(numerator.find(i));
		} else {
			denominator.insert(i);
		}

	}

	return true;
}

// binom(n, k) = (n!)/(k!)((n-k)!)

// "Multiplies" with n!, "divides" by k! and (n-k)!
bool Fraction::Multiply_Binom(int n, int k){
	size_t i;	// iterator

	// returns false if n is non-positive, or k is negative or bigger than n
	if(n <= 0) return false;
	if(k < 0 || k > n) return false;

	// adds n! to numerator
	for(i = n; i > 1; i--){
		if(denominator.find(i) != denominator.end()){
			denominator.erase(denominator.find(i));
		} else {
			numerator.insert(i);
		}
	}

	// adds k! to denominator
	for(i = k; i > 1; i--){
		if(numerator.find(i) != numerator.end()){
			numerator.erase(numerator.find(i));
		} else {
			denominator.insert(i);
		}
	}

	// adds (n-k)! to denominator
	for(i = (n - k); i > 1; i--){
		if(numerator.find(i) != numerator.end()){
			numerator.erase(numerator.find(i));
		} else {
			denominator.insert(i);
		}
	}

	return true;
}

// "Divides" by n!, "multiplies" by k! and (n-k)!
bool Fraction::Divide_Binom(int n, int k){
	size_t i;	// iterator

	// returns false if n is non-posotive, or if k is negative or greater than n
	if(n <= 0) return false;
	if(k < 0 || k > n) return false;

	// adds n! to denominator
	for(i = n; i > 1; i--){
		if(numerator.find(i) != numerator.end()){
			numerator.erase(numerator.find(i));
		} else {
			denominator.insert(i);
		}
	}

	// adds k! to numerator
	for(i = k; i > 1; i--){
		if(denominator.find(i) != denominator.end()){
			denominator.erase(denominator.find(i));
		} else {
			numerator.insert(i);
		}
	}

	// adds (n-k)! to numerator
	for(i = (n - k); i > 1; i--){
		if(denominator.find(i) != denominator.end()){
			denominator.erase(denominator.find(i));
		} else {
			numerator.insert(i);
		}
	}

	return true;
}

// "Inverts" fraction by swapping the multisets
void Fraction::Invert(){
	multiset <int> temp;

	temp = numerator;
	numerator = denominator;
	denominator = temp;

}

// Prints out fraction as string of multiplications then string of divisions
void Fraction::Print() const{
	multiset <int>::iterator nit, dit;	// iterators for numerator and denominator

	// outputs 1 for numerator if it is empty
	if(numerator.empty()){
		cout << "1";

		if(denominator.empty()){
			cout << endl;
			return;
		}
	}

	if(!numerator.empty()){
		// prints out first element
		nit = numerator.begin();
		while(*nit == 1) {
			nit++;
			if(nit == numerator.end()) break;
		}
		cout << *nit;
		// prints " * d" from second number on
		for( nit++; nit != numerator.end(); nit++)
			if(*nit != 1) cout << " * " << *nit << flush;
	}

	if(!denominator.empty()){
		// prints " / d" for all elements in denominator
		for( dit = denominator.begin(); dit != denominator.end(); dit++ )
			if(*dit != 1) cout << " / " << *dit;
	}

	// new line at end
	cout << endl;
}

// Calcualtes the value of the fraction
double Fraction::Calculate_Product() const{
	multiset <int>::iterator nit, dit;	// iterators
	long double top, bot;			// products of numerator and denominator

	// calculates the product of the numerator
	top = 1;
	for(nit = numerator.begin(); nit != numerator.end(); nit++)
		top *= *nit;

	// calculates product of denominator
	bot = 1;
	for(dit = denominator.begin(); dit != denominator.end(); dit++)
		bot *= *dit;

	// value is numerator/denomiator
	return (double) top/bot;
}
