// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; October 06, 2021
// This file calculates the odds and expected payout for a game of Keno. The file utilizes the fraction class to calculate the probabilities
// for the game. Bet, total pick, and payouts for catches are inputted on stdin

#include "fraction.hpp"
#include <cstdio>
#include <iostream>
#include <map>
using namespace std;

int main() {
	double bet, pay;			// intial bet, payout for certain catch
	int pick, cat;				// total pick, number caught for certain catch
	map <int, double> catchPayouts;		// all of the catches and their respective payouts
	map <int, double>::iterator cpit;	// iterator for ^
	Fraction f;				// Fraction used for calculating odds
	double odds, expPayout, totPayout;	// odds and associated payouts

	// pulls bet and pick numerber from stdin
	cin >> bet >> pick;

	// keeps pulling respective payout for a catch and adds it to map
	while(cin >> cat >> pay)
		catchPayouts.insert(make_pair(cat, pay));

	// prints bet and numer of balls picked
	printf("Bet: %.2f\n", bet);
	printf("Balls Picked: %d\n", pick);

	// totPayout is the sum of all expPayouts - bet, so we start we -bet
	totPayout = 0;

	// for each element in the map
	for( cpit = catchPayouts.begin(); cpit != catchPayouts.end(); cpit++){
		f.Clear();					// fraction cleared
		f.Multiply_Binom(80 - pick, 20 - cpit->first);	// calculates probability of
		f.Multiply_Binom(pick, cpit->first);		// cathching for element using
		f.Divide_Binom(80, 20);				// binom(80 - p, 20 -c) * binom(p, c) / binom(80, 20)

		odds = f.Calculate_Product();			// sets odds to print
		expPayout = odds * cpit->second;		// expPayout = odds * payout
		totPayout += expPayout;				// totPayout = sum(expPayouts) - bet

		// prints odds and expPayout for each element
		printf("  Probability of catching %d of %d: ", cpit->first, pick);
		printf("%g -- Expected return: %g\n", odds, expPayout);
	}

	totPayout -= bet;

	// prints total return and normed total return
	printf("Your return per bet: %.2lf\n", totPayout);
	printf("Normalized: %.2lf\n", totPayout / bet);

	return 0;
}
