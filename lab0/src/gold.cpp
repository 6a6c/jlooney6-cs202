// Jake Looney; jlooney6@vols.utk.edu; git & bitbucket: 6a6c; August 25, 2021
// This file reads in a "map" of characters through standard input that describes the location and ammount of gold. The chars correspond to 
// how many "ounces" of gold there are, with A being 1, B being 2, etc. The programs counts the total number of ounces of gold in the map
// and then outputs them at the end of the program.
#include <iostream>
using namespace std;

int main(){

	// variables
	int goldTotal;
	char c;

	goldTotal = 0;
	while(cin >> c){		//While cin is inputting chars, this loop continues.
		c -= 64; 		//Now, the value in c is offset by -64 so that A becomes 1.
		if(c <= 0) continue; 	//If A is . or -, it will be less than zero so will not be added to the total. So, the loop continues.
		goldTotal += c;		//The ounces of the char is added to the total.
	}

	cout << goldTotal << endl;

	return 0;
}
