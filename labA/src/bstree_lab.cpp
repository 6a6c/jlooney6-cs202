// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; November 17, 2021
// This file includes function definitons for Depth, Height, Ordered_Keys, the copy constructor, and
// assignment overload for the BSTree class.

#include <vector>
#include <string>
#include <iostream>
#include <cstdio>
#include "bstree.hpp"
using namespace std;
using CS202::BSTree;
using CS202::BSTNode;

int BSTree::Depth(const string &key) const {
    BSTNode *n;                                 // a node used for finding key in tree
    int depth;                                  // depth to be returned

    n = sentinel->right;                        // n starts at root of tree
    depth = 0;                                  // depth starts at depth of root (0)
    while(true){
        if ( n == sentinel ) return -1;         // if we reach the sentinel, key ain't in the tree
                                                // so we return -1
        if( n->key == key ) return depth;       // if n->key is key, we found the node, so we return depth

        depth++;                                // otherwise, depth is incremented
        n = ( key < n-> key ) ? n->left : n->right;     // and, we move closer towards key in the tree
    }
}

int BSTree::Height() const {
    BSTNode *n;                                 // a node used for finding base in tree
    int height;                                 // height to be returned

    n = sentinel->right;                        // n starts at root of tree
    height = recursive_find_height(n) + 1;      // calls recursive find height on the root
                                                // 1 is added to this, as the height of one node is one

    return height;
}

vector <string> BSTree::Ordered_Keys() const {
    vector <string> rv;

    make_key_vector(sentinel->right, rv);       // Calls make_key_vector on root of the tree

    return rv;
}

BSTree::BSTree(const BSTree &t){
    sentinel = new BSTNode;                     // Copy constructor starts by intializing instance
    sentinel->left = sentinel;                  // ala the regular constructor
    sentinel->right = sentinel;
    sentinel->parent = NULL;
    sentinel->key = "---SENTINEL---";
    sentinel->val = NULL;
    size = 0;

    *this = t;                                  // Then uses assingment overload to set equal to t.
}

BSTree& BSTree::operator= (const BSTree &t) {
    vector <string> keys;
    vector <void *> vals;
    BSTNode *n;                                 // n will become the new root of the tree

    keys = t.Ordered_Keys();                    // Sets keys and vals respectively
    vals = t.Ordered_Vals();

    this->Clear();                              // Clears this so we have an empty tree

    n =  this->make_balanced_tree(keys, vals, 0, keys.size() - 1); // CALLS WITH KEYS.SIZE() - 1 BECAUSE OF REDEFINITION
    n->parent = sentinel;                       // Points root parent to sentinel
    sentinel->right = n;                        // and points sentinel to root

    this->size = t.size;                        // sets size correctly

    return *this;
}

// Ternary expressions, baby!
int BSTree::recursive_find_height(const BSTNode *n) const {
    int height;                                 // height to be returned
    int leftHeight, rightHeight;                // the height of the left node and right node respecitvely

    // leftHeight and rightHeight are set to:
    //     - the Depth of the current node n, if the next left/right is the sentinel
    //     - the recursive_find_height of the left/right child, if next left/right isn't sentinel

    leftHeight = ( n->left == sentinel) ? Depth(n->key) : recursive_find_height(n->left);
    rightHeight = ( n->right == sentinel) ? Depth(n->key) : recursive_find_height(n->right);

    // finally, the height of the current node is set depending on which height is greater
    height = ( leftHeight > rightHeight ) ? leftHeight : rightHeight;

    return height;
}

void BSTree::make_key_vector(const BSTNode *n, vector<string> &v) const{
    if( n->left != sentinel ) make_key_vector(n->left, v);
    v.push_back(n->key);
    if( n->right != sentinel ) make_key_vector(n->right, v);
}

BSTNode *BSTree::make_balanced_tree(const vector<string> &sorted_keys,
                            const vector<void *> &vals,
                            size_t first_index,
                            size_t num_indices) const {

/*  Please be advised, I am reworking this function from what was laid out in the lecture notes because
 *  I find this to be far more intuitive. It still does the exact same thing.
 *
 *  Indices: 0 1 2 3 4 5 6 7                    The midpoint of the bounds of a set of
 *  Depth:           1                          indices will be the root that level's subtree.
 *(when its      2       2                      Because of this, I find passing the bounds throug the function
 * set as a    3   3   3   3                    (rather than first_index and num_indices) to be much
 *  root)    4                                  clear and easier to follow. If you don't like it, sue me.
 *
 *           In essence, num_indices is now just the left bound of a set of indices, instead of
 *           begin the size of that set of inidices.                                                           */

    BSTNode *rn;
    size_t midpoint, last_index;

    last_index = num_indices;           // for clarity, we will use last_index instead of num_indices

    if( last_index < first_index ) return sentinel;

    // if first_index-last_index is odd, that means that there is an even number
    // of elements to split. So, we take
    if( (last_index - first_index) % 2 == 1 ){
        midpoint = (first_index + last_index) / 2 + 1;
    }
    // otherwise, there is an odd number to split, so we divide normall.
    else{
        midpoint = (first_index + last_index) / 2;
    }

    // to avoid out of bounds
    if( midpoint >= sorted_keys.size() ) return sentinel;

    // sets the parent node to the key/vals at the midpoint index
    rn = new BSTNode;
    rn->key = sorted_keys.at(midpoint);
    rn->val = vals.at(midpoint);

    // sets left child to be the subtree with a root at the midpoint of the
    // section of indices between the first index and the left of the midpoint
    rn->left = make_balanced_tree(sorted_keys, vals, first_index, midpoint - 1);
    if( rn->left != sentinel ) rn->left->parent = rn;

    // sets right child to be the subtree with a root at the midpoint of the
    // section of indices between the right of the midpoint and the last index
    rn->right = make_balanced_tree(sorted_keys, vals, midpoint + 1, last_index);
    if( rn->right != sentinel ) rn->right->parent = rn;

    // returns rn, post order
    return rn;
}
