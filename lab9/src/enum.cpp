// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; November 8, 2021
// This file recursively outputs all binary strings of a certain length with a certain number of ones in order

#include <cstdio>
#include <string>
#include <sstream>
using namespace std;

void enumerate(string &s, int &index, int &numOnes){
    // sets index to 0 if negative
    if( index <= -1 ) {
        index = 0;
        return;
    }

    // prints string and sets index to s.length() - 1
    // if it is greater or equal to s.length()
    if( s.length() <= (unsigned) index ){
        printf("%s\n", s.c_str());
        index = s.length() - 1;
        enumerate(s, index, numOnes);
        return;
    }

    // if index is a #, then we want to iterate forward
    if(s[index] == '#'){
        // if there are no ones left available
        if( s.length() - index > (unsigned) numOnes ){
            s[index] = '0';     // set index to zero
            index++;            // increment and call function again
            enumerate(s, index, numOnes);
            return;
        } else {                // if there are ones left available
            s[index] = '1';     // set index to one
            index++;            // imcrement index
            numOnes--;          // decrement numOnes and call function again
            enumerate(s, index, numOnes);
            return;
        }
    }

    // if index is a zero and we have more ones, we iterate forward
    if(s[index] == '0' && numOnes != 0){
        s[index] = '1';         // sets index to ones
        numOnes--;              // decrements numOnes
        index++;                //increments index and calls function again
        enumerate(s, index, numOnes);
        return;
    // if index is a zero and we have no more ones, we iterate backwards
    } else if (s[index] == '0' && numOnes == 0){
        s[index] = '#';         // sets index to a #
        index--;                // increments index
        enumerate(s, index, numOnes);   // calls function again to finish chain
        return;
    }

    // if index is a one, it means we have already exhausted this chain/
    // so, we iterate backwards
    if(s[index] == '1'){
        s[index] = '#';         // sets index to a #
        index--;                // decrements index
        numOnes++;              // increments numOnes and calls function again
        enumerate(s, index, numOnes);
        return;
    }

}

int main(int argc, char** argv){
    string s;           // the string to be passed to enumerate
    int numOnes, index; // numOnes and index to be passed to enumerate
    istringstream ss;   // string stream for pulling out command line args
    size_t length, i;   // length is the length specified in command line
                        // i is an iterator

    // returns if incorrect command line args
    if(argc != 3){
        printf("Usage: bin/enum [length] [ones]\n");
        return -1;
    }

    // pulls length from command line
    ss.str(argv[1]);
    ss >> length;
    // pulls numOnes from command line
    ss.clear();
    ss.str(argv[2]);
    ss >> numOnes;

    // sets s to be length ammount of #'s
    s = "";
    for( i = 0; i < length; i++){
        s += "#";
    }

    // calls enumerate on index 0
    index = 0;
    enumerate(s, index, numOnes);

    return 0;

}
