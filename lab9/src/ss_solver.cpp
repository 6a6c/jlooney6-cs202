// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; November 8, 2021
// This file recursviely solves a game of ShapeShifter specified in command line arguments and on stdin.

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

class ShapeyShifty { // this is what happens when you let me name things
    public:
        ShapeyShifty(vector <string> startGrid);
        void UseShape(int index, int row, int col);
        bool Solve(int index);
        void PrintSols() const;

    protected:
        vector <string> Grid;
        vector <vector <string> > Shapes;
        vector <int> RowSols;
        vector <int> ColSols;
};

// constructor takes a starting grid and reads in Shapes from stdin
ShapeyShifty::ShapeyShifty(vector <string> startGrid){
    string s, s2;               // used to read from cin
    istringstream ss;           // used to read from cin
    vector <string> temp;       // used to add to Shapes

    // sets Grid to startGrid passed from cmdArgs
    Grid = startGrid;

    // reads a whole line from cin
    while(getline(cin, s)){
        // adds line to a stringstream
        ss.clear();
        ss.str(s);
        temp.clear();

        // pulls words off the string stream
        while(ss >> s2){
                // and adds them to temp
                temp.push_back(s2);
        }

        // temp now contains a whole shape, so it is pushed back onto Shapes
        Shapes.push_back(temp);

    }
}

// Uses the Shape at index and coordinates (doesn't check if valid, so only pass valid locations)
void ShapeyShifty::UseShape(int index, int row, int col){
    size_t i, j;        // iterators

    // iterates through the shape
    for( i = 0; i < Shapes.at(index).size(); i++ ){
        for( j = 0; j < Shapes.at(index).at(i).length(); j++){
            // sets to zero if both Grid and Shape have same value. otherwise, sets to 1
            if( Grid[i + row].at(j + col) == Shapes[index].at(i).at(j) ) Grid[i + row].at(j + col) = '0';
            else Grid[i + row].at(j + col) = '1';
        }
    }

}

// Solve recursively solves the game by testing all the possibilities
bool ShapeyShifty::Solve(int index){
    size_t i, j, k, l;          // iterators
    bool canUse, end;           // canUse is used to tell if a shape will fit at coordinates
                                // end is used to tell if the grid all ones

    // iterates through all spaces in the grid
    for( i = 0; i < Grid.size(); i++ ){
        for( j = 0; j < Grid[i].length(); j++){

            // canUse is set to true initially. if shape won't fit, it will be set to false
            canUse = true;
            if( Shapes[index].size() > Grid.size() - i ) canUse = false;
            for( k = 0; k < Shapes[index].size(); k++ ){
                if( Shapes[index].at(k).length() > Grid[0].length() - j ) canUse = false;
            }

            // if we canUse the shape, we use it. if not, we continue
            if ( canUse ) UseShape(index, i, j);
            else continue;

            // adds coordinates to Solution vector
            RowSols.push_back(i);
            ColSols.push_back(j);

            // end is set to true intially. if a 0 is found in Grid, it is set to false
            end = true;
            for( k = 0; k < Grid.size(); k++){
                for( l = 0; l < Grid[k].length(); l++)
                    if( Grid[k].at(l) == '0' ) end = false;
            }

            // special case if index is at the end of Shapes, because we can't make
            // another recursive call with out going out of range of Shapes
            if( (unsigned) index == Shapes.size() - 1 ){
                if(end) return true;            // if the Grid is 1's, return true
                else{
                    UseShape(index, i, j);      // puts Grid back into prev state
                    RowSols.pop_back();         // removes incorrect solution
                    ColSols.pop_back();         // removes incorrect solution
                    continue;
                }
            } else {
                if( end ) return true;          // if the Grid is 1's, return true
                if( Solve(index + 1) ) return true;     // if there exists some possible solution
                                                        // from this solution, return true
                else {                          // else
                    UseShape(index, i, j);      // puts Grid back into prev state
                    RowSols.pop_back();         // removes incorrect solution
                    ColSols.pop_back();         // removes incorrect solution
                    continue;
                }
            }
        }
    }

    // if we get here, we have tried every possible placement in Grid with no solution
    // so, returns false
    return false;
}

// PrintSols outputs the solution
void ShapeyShifty::PrintSols() const{
    size_t i, j;        // iterators

    for( i = 0; i < RowSols.size(); i++){
        // outputs the shape
        for( j = 0; j < Shapes[i].size(); j++ )
            cout << Shapes[i].at(j) << " ";

        // outputs the corresponding placement in solution vectors
        cout << RowSols[i] << " " << ColSols[i] << endl;
    }

}

int main(int argc, char** argv){
    vector <string> startGrid;  // starting grid from cmdArgs
    size_t i;                   // iterator

    // adds the grid from command args into startGrid
    for( i = 0; i < (unsigned) argc - 1; i++)
        startGrid.push_back(argv[i + 1]);

    // constructs a ShapeyShifty with startGrid
    ShapeyShifty s(startGrid);

    // if s is solvable, ouputs solution
    if( s.Solve(0) )
        s.PrintSols();

    return 0;
}
