// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 20, 2021
// This file is the implementation of the Hash202 class defined in include/hash_202.hpp. It is utilized by src/hash_tester.cpp.
// This file implements functions to set up a hash table, to add keys to the table, to find keys in the table, to
// print the table, and to count the total number of probes used by the table.

#include "hash_202.hpp"
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

// Hash functions:

int Last7Hash(const string &key){
	int hash;			// int for the hash to be returned
	istringstream ss;		// to pull hex out of key

	// if key is more than 7 digits, then the last 7 digits are inserted to ss
	// otherwise, the whole key is inserted to ss
	ss.clear();
	if(key.length() >= 7) ss.str(key.substr(key.length()-7));
	else ss.str(key);

	// the digits of key are put into the hash and returned
	ss >> hex >> hash;

	return hash;
}

int XorHash(const string &key){
	unsigned int hash, hash_n;	// hash is to be returned at the end
					// hash_n is used to calculate XOR of 7 digit chunks
	size_t i;			// iterator
	istringstream ss;		// used to pull hex out of key

	// pulls the first 7 digits out of key and sets it to hash
	ss.clear();
	ss.str(key.substr(0,7));
	ss >> hex >> hash;


	// if the key is longer than 7:
	if(key.length() > 7){

		// iterates through the key up until key.length() - (key.length() % 7)
		for(i = 1; i < key.length()/7; i++){

			// and pulls out the next 7 digits
			ss.clear();
			ss.str(key.substr((i * 7), 7));
			ss >> hex >> hash_n;

			// and XORs them with the previous set of XORed chunks
			hash ^= hash_n;

		}

		// if there are extra digits left over:
		if(key.length() % 7 != 0){

			// pulls the last chunk of digits out
			ss.clear();
			ss.str(key.substr(key.length() - (key.length() % 7)));
			ss >> hex >> hash_n;

			// and XORs them with the prvious XORed chunkks
			hash ^= hash_n;

		}

	}

	// now, hash contains all the XORed 7 digit chunks
	return hash;
}

// Member functions of Hash_202

// This funciton sets up the hash table based on size, hash function, and collision resolution strategy
string Hash_202::Set_Up(size_t table_size, const string &fxn, const string &collision){

	// Checks to make sure arguments passed are valid. If not, returns an error message
	if(!Keys.empty()) return "Hash table already set up";

	if(table_size == 0) return "Bad table size";

	if(fxn != "XOR" && fxn != "Last7") return "Bad hash function";

	if(collision != "Linear" && collision != "Double") return "Bad collision resolution strategy";


	// actually sets up the table
	if(fxn == "XOR") Fxn = 0;
	else if (fxn == "Last7") Fxn = 1;

	if(collision == "Linear") Coll = 0;
	else if (collision == "Double") Coll = 1;

	Keys.resize(table_size);
	Vals.resize(table_size);

	// returns empty string to indicate no errors
	return "";
}

// This function adds a key and its value into the table.
string Hash_202::Add(const string &key, const string &val){
	int hash, hash2, index;		// hash is used for H1(key)
					// if double hashing is used, hash2 is used for H2(key, i)
					// index is used for accessing table based on hash function
	size_t i;			// iterator
	stringstream ss;		// to pull hex digits out from key
	char c;				// to check if key is all hex digits

	// returns if no table
	if(Keys.size() == 0) return "Hash table not set up";

	// returns if no key/val is inputted
	if(key == "") return "Empty key";
	if(val == "") return "Empty val";

	// checks to make sure key is only hex digits. Returns if no
	ss.str(key);
	while(ss >> hex >> c){
		if(!(c >= '0' && c <= '9') && !(c >= 'a' && c <= 'f')
			&& !(c >= 'A' && c <= 'F')) return "Bad key (not all hex digits)";
	}

	// returns if hash table is completely full
	for(i = 0; i < Keys.size(); i++)
		if(Keys.at(i) == "") break;
	if(i == Keys.size()) return "Hash table full";

	// `Last7 hashing:
	if(Fxn == 1){

		// hashes key and indexes it in table at hash % table_size
		hash = Last7Hash(key);
		index = hash % Keys.size();

		// Collision resulotion:
		if(Keys.at(index) != ""){

			// if key at the index is the key we're hashing, returns
			if(Keys.at(index) == key) return "Key already in the table";

			// Linear probing:
			if(Coll == 0){

				// increments index until it finds an open space
				while(true){

					hash++;
					index = hash % Keys.size();
					if(Keys.at(index) == "") break;

				}

			}

			// Double hashing:
			else {

				// H2(key, i) = OtherHash(key) % table_size()
				hash2 = XorHash(key) % Keys.size();
				// if H2 is 0 (no change in index) it is set to 1
				if(hash2 == 0) hash2 = 1;

				i = 0;

				// keeps adding H2 to hash and recomputing index
				while(true){

					hash += hash2;
					index = hash % Keys.size();

					i++;

					// if we find a blank space, we break from the loop
					if(Keys.at(index) == "") break;

					// if we have incremented more times than table_size, then
					// we are stuck in a loop so we return with error
					if(i > Keys.size()) return "Cannot insert key";

				}

			}

		}

	}
	// XOR hashing: (follows identical structure to above)
	else {

		hash = XorHash(key);
		index = hash % Keys.size();

		// Collision resulotion:
		if(Keys.at(index) != ""){

			if(Keys.at(index) == key) return "Key already in the table";

			// Linear probing:
			if(Coll == 0){

				while(true){

					hash++;
					index = hash % Keys.size();
					if(Keys.at(index) == "") break;

				}

			}

			// Double hashing:
			else {

				hash2 = Last7Hash(key) % Keys.size();
				if(hash2 == 0) hash2 = 1;

				i = 0;

				while(true){

					hash += hash2;
					index = hash %  Keys.size();

					i++;;

					if(Keys.at(index) == "") break;

					if(i > Keys.size()) return "Cannot insert key";

				}

			}

		}

	}

	// Now, if we get to this point, then index is an open space in the hash table.
	// So, we insert the key and value into the table
	Keys.at(index) = key;
	Vals.at(index) = val;

	// Total number of keys incremented
	Nkeys++;

	// return empty string to indicate no errors
	return "";
}

// This function finds a key in the hash table by hashing the key and checking it against the key stored
string Hash_202::Find(const string &key){
	int hash, hash2, index;		// hash is used for the first hash of the key
					// hash2 is used for H2 is double hashing is selected
					// index is the index in the tablle based on the hash
	size_t i;			// iterator


	// Last7:
	if(Fxn == 1) {

		// hashes the key
		hash = Last7Hash(key);
		index = hash % Keys.size();

		// if the key doesn't equal the key stored, we check other indices based on collision strategy
		if(Keys.at(index) != key){

			// Linear probing
			if(Coll == 0){

				// adds one to index and checks if key hashed is key stored
				i = 0;
				while(true){

					hash++;
					index = hash % Keys.size();
					i++;

					if(Keys.at(index) == key) break;

					// if we've gone through the whole table, then
					// key must not be in table so we return empty string
					if(i > Keys.size()) return "";

				}

				// adds number of probes used to Nprobes
				Nprobes += i;

			}

			// Double hashing:
			else {
				// H2(key, i) = (OtherHash(key) % table_size) * i
				hash2 = XorHash(key) % Keys.size();
				if(hash2 == 0) hash2 = 1;

				// adds hash to index anc checks if key hashed is key stored
				i = 0;
				while(true){

					hash += hash2;
					index = hash % Keys.size();
					i++;

					if(Keys.at(index) == key) break;

					// if we've gone through the whole table, then
					// key must not be in table so we return empty string
					if(i > Keys.size()) return "";
				}

				// adds number of probes used to Nprobes
				Nprobes += i;
			}
		}
	}

	// XOR (follows identical format to above)
	else {

		hash = XorHash(key);
		index = hash % Keys.size();

		if(Keys.at(index) != key){

			// Linear probing:
			if(Coll == 0){

				i = 0;
				while(true){

					hash++;
					index = hash % Keys.size();
					i++;

					if(Keys.at(index) == key) break;
					if(i > Keys.size()) return "";

				}

				Nprobes += i;

			}
			// Double hashing:
			else {

				hash2 = Last7Hash(key) % Keys.size();
				if(hash2 == 0) hash2 = 1;

				i = 0;
				while(true){

					hash += hash2;
					index = hash % Keys.size();
					i++;

					if(Keys.at(index) == key) break;

					if(i > Keys.size()) return "";
				}

				Nprobes += i;
			}
		}

	}

	// now, index is the corresponding spot in the table for the key. So, we
	// return that index's value
	return Vals.at(index);
}

// This function prints out all table entries
void Hash_202::Print() const {
	size_t i;		// iterator

	// iterates through table size, only prints if there's an entry at i
	for(i = 0; i < Keys.size(); i++){

		if(Keys.at(i) == "") continue;
		else {
			cout << setw(5) << i << " " << Keys.at(i) << " " << Vals.at(i) << endl;
		}

	}


}

// This function returns the total number of probes needed for the entire table
size_t Hash_202::Total_Probes(){
	unsigned int  i;	// iterator

	// Nprobes is reset
	Nprobes = 0;

	// Then, for each non-empty index in the table, Find(Keys.at(index)) is called.
	// Find() increments Nprobes according to probes needed to find the key.
	for(i = 0; i < Keys.size(); i++){
		if(Keys.at(i) != "") this->Find(Keys.at(i));
	}

	// After all keys have been found, Nprobes contains correct number of probes
	return Nprobes;
}
