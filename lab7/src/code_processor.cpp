// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; October 18 2021
// This file is an implementation of the Code_Processor class defined in include/code_processor.hpp. It implements methods to add and remove 
// users, prizes, and phone numbers, to redemeem prizes and codes, and to write the state of the server to a file.

#include "code_processor.hpp"
#include <fstream>
using namespace std;

// The djb_hash from src/random_codes
unsigned int djb_hash(const string &s) {
  size_t i;
  unsigned int h;

  h = 5381;

  for (i = 0; i < s.size(); i++) {
    h = (h << 5) + h + s[i];
  }
  return h;
}

// The function creates a new instance of the prize class and adds a pointer to the Prizes map.
bool Code_Processor::New_Prize(const string &id, const string &description, int points, int quantity){
	Prize *p;	// the prize being created

	// returns if prize is in Prizes, if points is non-positive, or if quantity is non-positive
	if( Prizes.find(id) != Prizes.end() ) return false;
	if( points <= 0 ) return false;
	if( quantity <= 0) return false;

	// creates the prize and sets its members to passed vars
	p = new Prize;
	p->id = id;
	p->description = description;
	p->points = points;
	p->quantity = quantity;

	// adds the prize to Prizes
	Prizes.insert( make_pair(id, p) );

	return true;
}

// This function creates a new instance of the  user class and adds a pointer to it to the Users map
bool Code_Processor::New_User(const string &username, const string &realname, int starting_points){
	User *u;	// the user being created

	// returns if user is already in Names, or if starting_points is negative
	if( Names.find(username) != Names.end() ) return false;
	if( starting_points < 0 ) return false;

	// creates the user and sets its members to passed vars
	u = new User;
	u->username = username;
	u->realname = realname;
	u->points = starting_points;

	// adds the user to Names
	Names.insert( make_pair(username, u) );

	return true;
}

// This function finds a user in Names and erases and deletes the user
bool Code_Processor::Delete_User(const string &username){
	map <string, User *>::iterator uit;	// iterator to find user in Names
	set <string>::iterator phit;		// iterator to remove the phone numbers of the user

	// assigns uit to the found index of username
	uit = Names.find(username);

	// if name not found, returns false
	if( uit == Names.end() ) return false;

	// iterates through phones numbers associated w/ user and erases them from Phones
	for( phit = uit->second->phone_numbers.begin(); phit != uit->second->phone_numbers.end(); phit++){
		Phones.erase(Phones.find(*phit));
	}

	// deletes user
	delete uit->second;

	// erases user from users
	Names.erase(uit);

	return true;
}

// This function adds a phone to both the Phones map and to users' phone_numbers set
bool Code_Processor::Add_Phone(const string &username, const string &phone){
	map <string, User *>::iterator uit;	// iterator for  user in Names
	User *u;				// the usernames' User instance

	// assigns uit to the found index of username
	uit = Names.find(username);

	// returns false if username can't be found or if phone cant be found
	if( uit == Names.end() ) return false;
	if( Phones.find(phone) != Phones.end() ) return false;

	u = uit->second;

	// adds phone number to users' phone_numbers set
	u->phone_numbers.insert(phone);

	// adds phone-user pari to Phones map
	Phones.insert( make_pair(phone, u) );

	return true;
}

// This function removes a phone from being associated with a user
bool Code_Processor::Remove_Phone(const string &username, const string &phone){
	map <string, User *>::iterator uit;	// iterator for user in Names
	map <string, User *>::iterator phit;	// iterator for user in Phones
	User *u;				// the username's User instance

	// assigns uit and phit to found index of respective maps
	uit = Names.find(username);
	phit = Phones.find(phone);

	// if name or phone isn't in respective map, returns false
	if( uit == Names.end() ) return false;
	if( phit == Phones.end() ) return false;

	u = uit->second;

	// returns false if phone aint associated with User
	if( u->phone_numbers.find(phone) == u->phone_numbers.end() ) return false;

	u->phone_numbers.erase( u->phone_numbers.find(phone) );
	Phones.erase(phit);

	return true;
}

// This function outputs all of the phone numbers associated with a user
string Code_Processor::Show_Phones(const string &username) const{
	map <string, User *>::const_iterator uit;	// iterator for user in Names
	set <string>::iterator phit;			// iterator for phones in users' phone_numbers
	User *u;					// the username's User instance
	string ret;					// string to be returned

	// assigns uit to found index in Name
	uit = Names.find(username);

	// if name isnt in Names, returns 
	if( uit == Names.end() ) return "BAD USER";

	u = uit->second;

	// if there are no phones for the user, returns empty string
	if( u->phone_numbers.empty() ) return "";

	// iterates thru phone_numbers ands adds all of the numbers to return followed by new line
	ret = "";
	for( phit = u->phone_numbers.begin(); phit != u->phone_numbers.end(); phit++ ){
		ret += *phit;
		ret += "\n";
	}

	// returns the string of all phone numbers
	return ret;
}

// This function redeems a code for the user based off of username
int Code_Processor::Enter_Code(const string &username, const string &code){
	map <string, User *>::iterator uit;	// iterator for user in Names
	set <string>::iterator cit;		// iterator for code in Codes
	User *u;				// the username's User instance
	unsigned int hash;			// for hashing

	// assigns uit and cit to respective found indexes
	cit = Codes.find(code);
	uit = Names.find(username);

	// returns -1 if code is in Codes or username isnt in Names
	if( cit != Codes.end() ) return -1;
	if( uit == Names.end() ) return -1;

	u = uit->second;

	hash = djb_hash(code);

	// adds 10 to user if hash is divisible by 17, 3 if divisible by 13, and none for all others
	if( hash % 17 == 0 ){

		u->points += 10;
		Codes.insert(code);
		return 10;

	} else if( hash % 13 == 0) {

		u->points += 3;
		Codes.insert(code);
		return 3;

	} else return 0;
}

// This function redeems a code for user based off of phone number
int Code_Processor::Text_Code(const string &phone, const string &code){
	map <string, User *>::iterator phit;	// iterator for user in Phones
	set <string>::iterator cit;		// iterator for code in Codes
	User *u;				// the phone's User instance
	unsigned int hash;			// for hashing

	// assigns cit and phit to their respective found indexes
	cit = Codes.find(code);
	phit = Phones.find(phone);

	// returns if code is already in Codes or if phone isnt in Phones
	if( cit != Codes.end() ) return -1;
	if( phit == Phones.end() ) return -1;

	u = phit->second;

	hash = djb_hash(code);

	// adds 10 to user if hash is divisible by 17, 3 if divisible by 13, and none for all others
	if( hash % 17 == 0 ){

		u->points += 10;
		Codes.insert(code);
		return 10;

	} else if( hash % 13 == 0 ) {

		u->points += 3;
		Codes.insert(code);
		return 3;

	} else return 0;
}

// This function will mark a code as used, in order to backup server to saved state
bool Code_Processor::Mark_Code_Used(const string &code){
	// returns false if code is already in Codes
	if( Codes.find(code) != Codes.end() ) return false;
	// returns false if code is invalid
	if( djb_hash(code) % 17 != 0 && djb_hash(code) % 13 != 0 ) return false;

	// otherwise, inserts the code
	Codes.insert(code);

	return true;
}

// This function returns the user's balance
int Code_Processor::Balance(const string &username) const{
	map <string, User *>::const_iterator uit;	// iterator for user in Names

	// assigns uit to found index of username in Names
	uit = Names.find(username);

	// returns if user isn't in Names
	if( uit == Names.end() ) return -1;

	// returns user's points
	return uit->second->points;
}

// This function redeems a prize for a user based on their username
bool Code_Processor::Redeem_Prize(const string &username, const string &prize){
	map <string, Prize *>::iterator pit;	// iterator for prize in Prizes
	map <string, User *>::iterator uit;	// iterator for user in Names
	User *u;				// username's User instance
	Prize *p;				// prize-id's Prize instance

	// assigns uit and pit to found indexes of their respective maps
	pit = Prizes.find(prize);
	uit = Names.find(username);

	// returns false if username or prize aren't in Names/Prizes
	if( pit == Prizes.end() ) return false;
	if( uit == Names.end() ) return false;

	p = pit->second;
	u = uit->second;

	// returns false if user does not have enogh poitnts to redeem prize
	if( u->points < p->points ) return false;

	// decremeents user's points by prize's cost and prize's quantity by one
	u->points -= p->points;
	p->quantity -= 1;

	// if prize's quantity is now 0, deletes and erases prize
	if( p->quantity <= 0 ) {
		Prizes.erase(pit);
		delete p;
	}

	return true;
}

// The destructor deletes all the instances created
Code_Processor::~Code_Processor(){
	map <string, User *>::iterator uit;	// iterator for users in Names
	map <string, Prize *>::iterator pit;	// iterator for prizes in Prizes

	// iterates through Names and deletes all pointers
	for( uit = Names.begin(); uit != Names.end(); uit++ )
		delete uit->second;

	// iterates through Prizes and delets all pointers
	for( pit = Prizes.begin(); pit != Prizes.end(); pit++ )
		delete pit->second;

	// clears all maps/sets
	Names.clear();
	Prizes.clear();
	Phones.clear();
	Codes.clear();
}

// This function writes the server's current state to a file 
bool Code_Processor::Write(const string &filename) const{
	map <string, User *>::const_iterator uit;	// iterator for users in Names
	map <string, Prize *>::const_iterator pit;	// iterator for prizes in Prizes
	map <string, User *>::const_iterator phit;	// iterator for users in Phones
	set <string>::const_iterator phit2;		// iterator for phones in phone_numbers
	set <string>::const_iterator cit;		// iterator for codes in Codes
	ofstream fout;					// file stream

	// opens filename, retruns false if fails
	fout.open(filename.c_str());
	if( fout.fail() ) return false;

	// iterates through Prizes and creates a PRIZE command for each prize
	for( pit = Prizes.begin(); pit != Prizes.end(); pit++ ){
		fout << "PRIZE      ";
		fout << pit->second->id << " ";
		fout << pit->second->points << " ";
		fout << pit->second->quantity << " ";
		fout << pit->second->description << endl;
	}

	// iterates through Names and creates a ADD_USER command for each user
	for( uit = Names.begin(); uit != Names.end(); uit++ ){
		fout << "ADD_USER   ";
		fout << uit->second->username << " ";
		fout << uit->second->points << " ";
		fout << uit->second->realname << endl;
	}

	// iterates through Phones and
	for( phit = Phones.begin(); phit != Phones.end(); phit++ ){
		// creates an ADD_PHONE command for each phone in user's phone_numbers
		for( phit2 = phit->second->phone_numbers.begin(); phit2 != phit->second->phone_numbers.end(); phit2++ ){
			fout << "ADD_PHONE ";
			fout << phit->second->username << " ";
			fout << *phit2 << endl;;
		}

	}

	// iterates through Codes and creates a MARK_USED command for each code
	for( cit = Codes.begin(); cit != Codes.end(); cit++ ){
		fout << "MARK_USED ";
		fout << *cit << endl;
	}

	// closes file and returns
	fout.close();

	return true;
}
