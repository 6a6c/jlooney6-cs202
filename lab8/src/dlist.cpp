// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; November 1, 2021
// This file implements a doubly-linked list by implementing the Dnode and Dlist classes found in include/dlist.hpp.
// It carries all the functionality of the STL doubly-linked list, with the added bonus that Pop_Front and Pop_Back
// return strings of the delelted nodes.

#include "dlist.hpp"
using namespace std;

// returns pointer to next node (flink)
Dnode* Dnode::Next(){
    return flink;
}

// returns pointer to prev node (blink)
Dnode* Dnode::Prev(){
    return blink;
}

// Constructor creates a new sentinel, and points its flink/blink to itself. Then, sets size to 0.
Dlist::Dlist(){
    sentinel = new Dnode;
    sentinel->s = "SENTINEL";
    sentinel->flink = sentinel;
    sentinel->blink = sentinel;

    size = 0;
}

// Destructor deletes all nodes in the list and then deleletes the sentinel.
Dlist::~Dlist(){
    Dnode *dit, *next;  // two pointers needed because of delele call

    // iterates through Dlist and deletes all nodes
    for( dit = this->Begin(); dit != this->End(); dit = next){
        next = dit->Next();
        delete dit;
    }

    // deletes sentinel; all memory allocated has been freed.
    delete sentinel;

    size = 0;
}

// Operator overload for assignment
Dlist& Dlist::operator= (const Dlist &d){
    Dnode *dit; // to iterate through d

    // clears this, so we have a fresh, empty list
    Clear();

    // Pushes back all elements of d
    for( dit = d.Begin(); dit != d.End(); dit = dit->Next() ){
        Push_Back(dit->s);
    }

    // returns
    return *this;
}

// Copy constructor
Dlist::Dlist(const Dlist &d){
    // initializes new Dlist ala the regular constructor
    sentinel = new Dnode;
    sentinel->s = "SENTINEL";
    sentinel->flink = sentinel;
    sentinel->blink = sentinel;
    size = 0;

    // uses assignment overload to copy
    *this = d;
}

// Basically the same thing as the destructor but doesn't delete sentinel
void Dlist::Clear(){
    Dnode *dit, *next;  // two pointers needed because of delete call

    // iterates through Dlist and delets all nodes
    for( dit = this->Begin(); dit != this->End(); dit = next){
        next = dit->Next();
        delete dit;
    }

    // sets sentinel to point to itself in both directions
    sentinel->flink = sentinel;
    sentinel->blink = sentinel;

    // set size to 0
    size = 0;
}

// Returns true if empty, false if not
bool Dlist::Empty() const{
    return (size == 0);
}

// This is a really complicated procedure, so I'm not going to describe it
size_t Dlist::Size() const{
    return size;
}

// Adds new element at end of Dlist
void Dlist::Push_Back(const string &s){
    Dnode *newBack;     // the new element

    // Creates new dnode and sets value
    newBack = new Dnode;
    newBack->s = s;

    // sets new elements's blink and flink to fit into the list
    newBack->blink = sentinel->Prev();
    newBack->flink = sentinel;

    // sets neighboring elements blink and flink to fit new element into list
    newBack->Prev()->flink = newBack;
    sentinel->blink = newBack;

    // incremenets size
    size++;
}

// Adds a new element at beginning of list.
void Dlist::Push_Front(const string &s){
    Dnode *newFront;    // the new element

    // creates new Dnode and sets its value
    newFront = new Dnode;
    newFront->s = s;

    // sets new element's flink and blink to fit into the list
    newFront->blink = sentinel;
    newFront->flink = sentinel->Next();

    // sets neighboring elemenents blink and flink to new element
    newFront->Next()->blink = newFront;
    sentinel->flink = newFront;

    // increments size
    size++;
}

// removes the last element from the list
string Dlist::Pop_Back(){
    string ret;
    Dnode *newBack;

    if(size == 0) return "";

    ret = sentinel->Prev()->s;

    newBack = sentinel->Prev()->Prev();

    newBack->flink = sentinel;

    delete sentinel->Prev();

    sentinel->blink = newBack;

    size--;

    return ret;

}

// Removes the first elemnent from the list
string Dlist::Pop_Front(){
    string ret;
    Dnode *newFront;

    if(size == 0) return "";

    ret = sentinel->Next()->s;

    newFront = sentinel->Next()->Next();

    newFront->blink = sentinel;

    delete sentinel->Next();

    sentinel->flink = newFront;

    size--;

    return ret;
}

// returns first element
Dnode* Dlist::Begin() const{
    return sentinel->flink;
}

// returns last element
Dnode* Dlist::Rbegin() const{
    return sentinel->blink;
}

// Returns one after last element, aka sentinel
Dnode* Dlist::End() const{
    return sentinel;
}

// Returns one before first element, aka sentinel
Dnode* Dlist::Rend() const{
    return sentinel;
}

// Inserts element before element n
void Dlist::Insert_Before(const string &s, Dnode *n){
    Dnode *insert;      // element to insert

    // creates new Dnode and set's its value
    insert = new Dnode;
    insert->s = s;

    // sets new element's flink and blink to fit into the list
    insert->flink = n;
    insert->blink = n->Prev();

    // set's neighboring elemnent's flink and blink to new element
    n->Prev()->flink = insert;
    n->blink = insert;

    // increments size
    size++;
}

// Inserts element after element n
void Dlist::Insert_After(const string &s, Dnode *n){
    Dnode *insert;      // element to insert

    // creates new Dnode and set's is value
    insert = new Dnode;
    insert->s = s;

    // sets new element's flink and blink to fit into the list
    insert->blink = n;
    insert->flink = n->Next();

    // setes neighboring element's flink and blink to new elemnet
    n->Next()->blink = insert;
    n->flink = insert;

    // increments size
    size++;
}

// Erases element n
void Dlist::Erase(Dnode* n){
    // sets neighboring elements to now point to each other
    n->Prev()->flink = n->Next();
    n->Next()->blink = n->Prev();

    // deletes element
    delete n;

    // decrements size
    size--;
}
