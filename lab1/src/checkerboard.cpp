// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; August 29, 2021
// This file takes user input through standard input and outputs a corresponding checkerboard to standard output. Input is formatted
// rows, columns, starting character, cycle size, width. Rows and columns determine the number of squares on the checkerboard,
// while starting character, cycle size, and width determine how it is output.

#include <cstdio>
#include <iostream>
using namespace std;

int main(){
	int rows, cols, cycleSize, width;	// user input for rows, columns, cycle size and square width
	int i, j, x, y;				// iterators
	char startChar;		// user input for starting char
	char* matrix;

	cin >> rows >> cols >> startChar >> cycleSize >> width;
	if(cin.fail()){
		cerr << "usage: checkerboard  - stdin should contain R, C, SC, CS and W" << endl;
		return -1;
	}
	// exits silently if any parematers are less than or equal to 0
	if((width <= 0) || (cols <= 0) || (cycleSize <= 0) || (width <= 0) || (startChar <= 0)) return -1;
	// or if startChar + cycleSize is greater than 127
	if((startChar + cycleSize)  > 127) return -1;

	// creates new matrix to input the chars into
	matrix = new char[rows * cols];

	// assigns the value of matrix[j][i] according to the formula
	// currChar = SC = + (i+j)%CS
	for(j = 0; j < rows; j++){
		for(i = 0; i < cols; i++){
			matrix[(j*cols) + i] = startChar + (i + j)%cycleSize;
		}
	}

	// prints the matrix extended according to the user inputted width
	for(j = 0; j < rows; j++){
		for(y = 0; y < width; y++){
			for(i = 0; i < cols; i++){
				for(x = 0; x < width; x++){
					cout << matrix[(j * cols) + i];
				}
			}
			cout << endl;
		}
	}

	// no hanging pointers!
	delete[] matrix;

	return 0;

}
