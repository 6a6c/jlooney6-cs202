// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; August 30, 2021
// This file takes input from technology expert and sage Moonglow's grade files through standard input. The students name is
// always preceded by "NAME". A group of numbers to average is preceded by "AVERAGE". At the end, the student's name
// and final exam score is output to standard out.

#include <iostream>
using namespace std;

int main(){
	double questionScore, averageScore, averageTot, averageCount, examScore;	// for question scores or question averages
	bool averaging;									// for telling if a number is a score or an average
	string s, name;									// to track inputted strings and name

	examScore = 0.0;
	while(true){
		// resets question score to 0.0 before any input
		questionScore = 0.0;

		// takes input for the question score
		cin >> questionScore;
		// if input is not a number, then:
		if(cin.fail()){
			// cin is cleared and input is held in string s
			cin.clear();
			cin >> s;

			// if input was "AVERAGE", then the averaging boolean is set so that
			// averaging process will input the next stream of numbers
			if(s == "AVERAGE") averaging = true;

			// if input was "NAME", then the next string is input into string name
			if(s == "NAME") cin >> name;
		}
		// if cin is at end of file, break from the  loop to output
		if(cin.eof()) break;

		// if the averaging boolean is set
		if(averaging){
			// all variables used to calculate averages set to 0
			averageTot =  0.0;
			averageScore = 0.0;
			averageCount = 0.0;

			while(true){
				// read cin into averageScore
				cin >> averageScore;
				// if input is not a number, then:
				if (cin.fail()){
					// clear cin, set the averaging to false, and break out of this loop
					cin.clear();
					averaging = false;
					break;
				}

				// otherwise, add the number inputted to averageTot and increment averageCount
				averageTot += averageScore;
				averageCount += 1;
			}

			// if no divide by zero, set questionScore to be the average
			if(averageCount != 0) questionScore = (averageTot / averageCount);
		}

		// at the end, add the questionScore to the exam score
		examScore += questionScore;
	}

	// after exam score has been calculated, outputs name followed by examScore
	cout << name << " " << examScore << endl;

	return 0;
}
