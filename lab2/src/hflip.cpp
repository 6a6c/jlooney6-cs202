// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 6, 2021
// This file takes input from a PGM file and flips it horizontally about the middle. It utilizes the PGM class implemented in src/pgm.cpp.
// PGM::Input() creates the file from std in. Then, this file filps the pixArray horizontally. This is restored into the PGM file and
// PGM::Output() outputs the file std out.

#include "pgm.cpp"
using namespace std;

int main(){
	int* tempPix;		// an array used to copy and flip the pixArray
	int i, j;		// iterators

	PGM pgm;		// creates PGM pgm
	pgm.Input();		// and fills it from input

	// if badFile flag is set, return. error statement will be made on cerr
	if(pgm.badFile) return -1;

	// creates new array for tempPix
	tempPix = new int[pgm.width * pgm.height];

	// iterates thru the pixel array and flips it horizontally in tempPix
	for(j = 0; j < pgm.height; j++){
		for(i = 0; i < pgm.width; i++){	
			// tempPix stores value of the mirror pixel from pixArray
			tempPix[((j + 1) * pgm.width) - (i + 1)] = pgm.pixArray[i + (j * pgm.width)];
		}
	}
	// pgm.pixArray is deleted and replaced with tempPix
	delete[] pgm.pixArray;
	pgm.pixArray = tempPix;

	// outputs pgm
	pgm.Output();

	return 0;
}
