// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 6, 2021
// This file outputs information about a PGM file read from std in. It utilizes the PGM class which is implemented in src/pgm.cpp.
// PGM::Input() reads in the PGM. Then, this file outputs the number of rows, columns, and pixels, as well as the average
// pixel value.

#include "pgm.cpp"
using namespace std;

int main(){
	int i;			// iterator
	int pixelValTot;	// to calculate average pixel value

	PGM pgm;		// creates PGM pgm
	pgm.Input();		// and reads input into pgm

	// if badFile is set, returns -1. error statement will be made on cerr
	if(pgm.badFile)	return -1;

	// iterates through the pixel array and adds up all the pixel values.
	pixelValTot = 0;
	for(i = 0; i < (pgm.width*pgm.height); i++){
		pixelValTot += pgm.pixArray[i];
	}

	// prints out information about the file
	printf("# Rows:%12d\n", pgm.height);
	printf("# Columns:%9d\n", pgm.width);
	printf("# Pixels:%10d\n", pgm.pixelsRead);
	printf("Avg Pixel:%9.3lf\n", (double) pixelValTot/(pgm.width * pgm.height));

	return 0;
}
