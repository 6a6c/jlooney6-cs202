#include "pgm.cpp"
#include <cmath>
using namespace std;

int main(){	
	int i, j;

	PGM pgm;
	pgm.Input();

	if(pgm.badFile){	
		return -1;
	}

	for(j = 0; j < pgm.height; j++){
		for(i = 0; i < pgm.width; i++){
			pgm.pixArray[i + (j * pgm.width)] = abs(pgm.pixArray[i + (j * pgm.width)] - 255);
		}
	}	

	pgm.Output();

	return 0;
}
