// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 6, 2021
// This file contains the implementations for the PGM class. It contains member functions implementations to create
// PGM from input on std in, to create a blank one from input in command line args, and to output to std out.
// It is defined in include/pgm.h.

#include "pgm.h"
#include <iostream>
#include <cstdio>
using namespace std;

//constructor sets all variables to 0
PGM::PGM(){
	width = 0;
	height = 0;
	pixArray = 0;
	badFile = false;
}

//destructor deletes pixArray
PGM::~PGM(){
	delete[] pixArray;
}

//creates PGM from input on stdin
void PGM::Input(){
	string p2;		// for testing first word
	int w, h;		// for width, height
	int shouldBe255;	// for testing 4th word
	int temp;		// for reading in pixels

	// gets the first line and breaks if it is not "P2"
	cin >> p2;
	if(p2 != "P2"){
		badFile = true;
		cerr << "Bad PGM file -- first word is not P2" << endl;
		return;
	}

	// gets the column spec and breaks if cin fails or spec is less than 0
	cin >> w;
	if(cin.fail() || w < 0){
		cin.clear();
		badFile = true;
		cerr << "Bad PGM file -- No column specification" << endl;
		return;
	}
	// sets this.width to w
	width = w;

	// gets the row spec and breaks if cin fails or spec is less than 0
	cin >> h;
	if(cin.fail() || h < 0){
		cin.clear();
		badFile = true;
		cerr << "Bad PGM file -- No row specification" << endl;
		return;
	}
	// sets this.height to h
	height = h;

	// gets fourth word and breaks if not 255
	cin >> shouldBe255;
	if(cin.fail() || shouldBe255 != 255){
		cin.clear();
		badFile = true;
		cerr << "Bad PGM file -- No 255 following the rows and columns" << endl;
		return;
	}

	// creates new array for pixArray
	pixArray = new int[width * height];

	pixelsRead = 0;
	// reads each pixel into temp, keeps going until cin fails or eofs
	while(cin >> temp){
		// if cin fails, loop is exited and bad file set to true
		if(cin.fail()){
			cin.clear();
			badFile = true;
			cerr << "Bad PGM file -- Cin failed" << endl;
			break;
		}

		// if pixel read is not between 0 and 255, pixel read is added to badPixels vector
		if(temp > 255 || temp < 0){
			badPixels.push_back(pixelsRead);
		
		}

		// temp is stored into pixArray if there is room
		if(pixelsRead < (width * height)) pixArray[pixelsRead] = temp;

		pixelsRead++;
	}

	// if more pixels were read than specified, badFile is set and function returns
	if(pixelsRead > (width * height)){
		badFile = true;
		cerr << "Bad PGM file -- Extra stuff after the pixels" << endl;
		return;
	}

	// if badPixels were read, badFile is set and function returns
	if(badPixels.size() !=  0){
		badFile = true;
		cerr << "Bad PGM file -- pixel " << badPixels.at(0) << " is not a number between 0 and 255" << endl;
		return;
	}

}

// creates a PGM file with width w, height h, and filled with pixels of value fill
void PGM::CreateBlank(int w, int h, int fill){
	int i;		// iterator

	// this.width and this.height set
	width = w;
	height = h;

	// new array for pixArray created
	pixArray = new int[width*height];

	// pixArray populated with fill
	for(i = 0; i < (width * height); i++){
		pixArray[i] = fill;
	}
}

// ouputs the PGM file to standard out
void PGM::Output(){
	int i;		// iterator

	// prints out the first 3 lines
	printf("P2\n");
	printf("%d %d\n", width, height);
	printf("255");

	// then prints out the pixArray, with new lines every new row
	for(i = 0; i < (width * height); i++){
		if(i%width == 0) printf("\n");
		printf("%3d ", pixArray[i]);
	}
}
