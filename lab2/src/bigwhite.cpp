// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 6, 2021
// This file creates and outputs a PGM filed from command line arguments. It utilizes the PGM class implemented in src/pgm.cpp.
// PGM::CreateBlank() creates the PGM file based on the rows and columns specified in command line args. Then the file is output
// using PGM::Output().

#include "pgm.cpp"
#include <sstream>
using namespace std;

int main(int  argc, char* argv[]){
	int rows, cols;		// rows and cols from command line
	stringstream ss;	// to input rows and cols

	// if theres not 3 arguments, error statement and return
	if(argc != 3){
		cerr << "usage: bigwhite rows cols" << endl;
		return -1;
	}

	// rows is grabbed from command line args
	ss.str(argv[1]);
	ss >> rows;
	ss.clear();

	// cols is grabbed from command line args
	ss.str(argv[2]);
	ss >> cols;

	// if rows or cols 0 or less, error statement and return
	if(rows <= 0 || cols <= 0){
		cerr << "usage: bigwhite rows cols" << endl;
		return -1;
	}

	PGM pgm;				// creates PGM pgm
	pgm.CreateBlank(cols, rows, 255);	// fills pgm with 255 (white)

	pgm.Output();				// outputs pgm

	return 0;
}
