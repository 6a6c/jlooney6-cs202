// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; September 6, 2021
// This file contains the definitions for the PGM class. It contains members for height, width, the pixel array, as well as error checking.
// It contains member functions to create PGM from input, to create a blank one, and to output.
// It is implemented in src/pgm.cpp.

#ifndef _PGM_CLASS_H
#define _PGM_CLASS_H

#include <vector>
#include <string>
using namespace std;

class PGM {
	public:
		PGM();						// constructor takes no arguments and will set variables to 0
		~PGM();						// destructor will delete pixel array
		void Input();					// will take input from cin to create the PGM
		void CreateBlank(int w, int h, int fill);	// will make PGM with width w, height h, filled with pixels of fill value
		void Output();					// outputs PGM file to std out

		int width, height;				// height and width of picture
		int* pixArray;					// will contain the pixel values

		bool badFile;					// to turn true if something in file is corrupted
		vector <int> badPixels;				// to track location of bad pixels
		int pixelsRead;					// to track pixels read from std in
};


#endif
