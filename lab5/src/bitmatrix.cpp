// Jake Looney; jlooney6@vols.utk.edu; bitbucket.org/6a6c; October 4, 2021
// This file implements all the Bitmatrix and BM_Hash() class from include/bitmatrix.hpp. Instances of the class will be created
// in bitmatrix_editor and the functions from here will be utilized to create, print, write, edit, add, multiply, and invert bitmatrices.

#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include "bitmatrix.hpp"
using namespace std;

// This constructor creates a Bitmatrix of 0's with the rows, cols specified
Bitmatrix::Bitmatrix(int rows, int cols) {
	int i;			// iterator
	string emptyRow;	// a string corresponding to a row of 0's

	// throws exception for negatve rows/cols
	if(rows <= 0) throw((string) "Bad rows");
	if(cols <= 0) throw((string) "Bad cols");

	// makes emptyRow a string of cols 0's
	emptyRow = "";
	for(i = 0; i < cols; i++){
		emptyRow += "0";
	}

	// pushes back an emptyRow for each row
	for(i = 0; i < rows; i++){
		M.push_back(emptyRow);
	}
}

// This constructor creates a Bitmatrix based on input from a file fn
Bitmatrix::Bitmatrix(const string &fn) {
	ifstream fin;			// file of matrix we are reading
	string rowRead, rowWrite;	// used to read and write to M vector
	unsigned int prevLength;	// used to ensure matrix is rectangular

	// opens file and throws exception if file name doesn't exist
	fin.open(fn.c_str());
	if(fin.fail()) {
		throw((string) "Can't open file");
		return;
	}


	// reads the matrix from file using getline and stringstream
	prevLength = 0;
	while(getline(fin, rowRead)){
		stringstream ss;	// for extracting chars from rowRead
		string s;

		ss.str(rowRead);

		// skips blank lines
		if(rowRead.length() == 0) continue;

		// chars pulled from rows read and added to rowWrite, whitespace skipped
		rowWrite = "";
		while(ss >> s) rowWrite += s;

		// if rowWrite is not the length of original row, exception is thrown
		if(rowWrite.length() != prevLength && prevLength != 0) throw((string) "Bad file format");
		prevLength = rowWrite.length();

		// pushes back rowWrite into M
		if(rowWrite != "") M.push_back(rowWrite);
	}

	fin.close();
}

// Copy creates a new Bitmatrix identical to this and returns a pointer to it
Bitmatrix *Bitmatrix::Copy() const {
	Bitmatrix *copy;

	copy =  new Bitmatrix(M.size(), M[0].length());

	copy->M = M;

	return copy;
}

// This function writes the bitmatrix to a file fn
bool Bitmatrix::Write(const string &fn) const {
	ofstream fout;		// the file we are writing to
	unsigned int i;		// iterator

	// opens fn and throws exception if fail
	fout.open(fn.c_str());
	if(fout.fail()) return false;

	// adds the entire string of M.at(i) for all rows in M
	for(i = 0; i < M.size(); i++){
		fout << M.at(i) << endl;
	}

	fout.close();

	return true;
}

// This method prints to stdout with a space every w chars
void Bitmatrix::Print(size_t w) const {
	unsigned int i, j;	// iterators

	// if w is 0, w is set to either rows or cols, whichever is greater
	if(w <= 0 && M[0].length() >= M.size()) w = M[0].length();
	else if(w <= 0 && M[0].length() < M.size()) w = M.size();

	// prints out every row
	for(i = 0; i < M.size(); i++){

		// prints a new line for a space if needed
		if(i % w == 0 && i != 0 && i != M.size())  printf("\n");

		// prints out the row with spaces when needed
		for(j = 0; j < M[i].length(); j++){
			if(j % w == 0 && j != 0 && j != M[i].length()) printf(" ");
			printf(M.at(i).substr(j, 1).c_str());
		}

		// new line at the end of every row
		printf("\n");

	}

}

// This function creates a PGM file fn where each element of M is p x p pixels and with a border around
// eacg elemenet of border width
bool Bitmatrix::PGM(const string &fn, int p, int border) const {
	ofstream fout;		// the file we are writing to
	int i, j, k, l;		// iterators
	int pixRows, pixCols;	// total pixels
	int pixVal;		// 100 for 1, 255 for 0

	// opens file and throws exception if failed
	fout.open(fn.c_str());
	if(fout.fail()) return false;

	// calculates total pixels in cols/rows
	pixRows = (M.size() * p) + ((M.size() + 1) * border);
	pixCols = (M[0].length() * p) + ((M[0].length() + 1) * border);

	// outputs PGM header
	fout << "P2" << endl;
	fout << pixCols << " " << pixRows << endl;
	fout << 255 << endl;

	// outputs the border at the top of the image
	for(k = 0; k < border; k++){
		for(i = 0; i < pixCols; i++){
			fout << 0 << " ";
		}
		fout << endl;
	}

	// for every row in M:
	for(j = 0; (unsigned) j < M.size(); j++){

		// repeates same row p times
		for(i = 0; i < p; i++){

			// adds a border for the left border
			for(k = 0; k < border; k++)
				fout << 0 <<  " ";

			// adds the whole row after border
			for(l = 0; (unsigned) l < M[0].length(); l++){
				if(this->Val(j, l) == '1') pixVal = 100;
				else pixVal = 255;

				// prints a width of pixels
				for(k = 0; k < p; k++){
					fout << pixVal << " ";
				}

				// prints a width of border
				for(k = 0; k < border; k++)
					fout << 0 << " ";
			}

			fout << endl;
		}

		// and outputs border at the end of the row
		for(k = 0; k < border; k++){
			for(i = 0; i < pixCols; i++)
				fout << 0 << " ";

			fout << endl;
		}

	}

	fout.close();

	return true;
}

// returns num rows
int Bitmatrix::Rows() const {
	return M.size();
}

// returns num cols
int Bitmatrix::Cols() const {
	return M[0].length();
}

// This function returns the value at M[row][col]
char Bitmatrix::Val(int row, int col) const {
	// retruns 'x' if row/col exceed matrix bounds
	if((unsigned) row + 1 > M.size() || (unsigned) col + 1 > M[0].length()) return 'x';
	if(row < 0 || col < 0) return 'x';

	// returns '0' or '1' based on char at col of string M[row]
	if(M[row].at(col) == '0') return '0';
	else if(M[row].at(col) == '1') return '1';
	else return 'x';
}

// This function sets M[row][col] to val
bool Bitmatrix::Set(int row, int col, char val) {
	string rowStr;	// the string at M[row]

	// returns false if row/col exceed matrix bounds
	if(row < 0 || (unsigned) row >= M.size()) return false;
	if(col < 0 || (unsigned) col >= M[0].length()) return false;

	// rowStr is set to string a M[row]
	rowStr = M.at(row);

	// char at col is set based on val
	if(val == 0 || val == '0') {
		rowStr.at(col) = '0';
		M.at(row) = rowStr;
		return true;
	} else if(val == 1 || val == '1') {
		rowStr.at(col) = '1';
		M.at(row) = rowStr;
		return true;
	} else return false;
}

// This function swaps two rows in the matrix
bool Bitmatrix::Swap_Rows(int r1, int r2) {
	string row1, row2;	// the two rows to swap

	// returns false if rows exceed bounds of matrix
	if(r1 < 0 || r2 < 0 || (unsigned) r1 >= M.size() || (unsigned) r2 >= M.size()) return false;

	// row1/2 set to corresponding row then swapped
	row1 = M.at(r1);
	row2 = M.at(r2);

	M.at(r1) = row2;
	M.at(r2) = row1;

	return true;
}

// This function adds the value of row r2 to row r1
bool Bitmatrix::R1_Plus_Equals_R2(int r1, int r2) {
	string row1, row2, add;	// the two original rows and the sum of the two rows
	unsigned int i;		// iterator

	// returns false is r1 or r2 exceed bounds of matrix
	if(r1 < 0 || r2 < 0 || (unsigned) r1 >= M.size() || (unsigned) r2 >= M.size()) return false;

	// row1/2 set to original rows
	row1 = M.at(r1);
	row2 = M.at(r2);

	// add is set to zero
	add = "";

	// add is appended based on the sum between values in row1 and row2 at char i
	for(i = 0; i < row1.length(); i++){
		if(row1.at(i) == '1' && row2.at(i) == '0') add += '1';
		else if(row1.at(i) == '0' && row2.at(i) == '1') add += '1';
		else add += '0';
	}

	// add now contains the sum, so r1 in the matrix is set to sum
	M.at(r1) = add;

	return true;
}

// This function adds two matrices together and returns a pointer the sum matrix
Bitmatrix *Sum(const Bitmatrix *a1, const Bitmatrix *a2) {
	Bitmatrix *sum;	// sum matrix to be returned
	int i, j;	// iterators

	// returns NULL if matrices don't have same num of rows and cols
	if(a1->Rows() != a2->Rows()) return NULL;
	if(a1->Cols() != a2->Cols()) return NULL;

	sum = new Bitmatrix(a1->Rows(), a1->Cols());

	// iterates through each element and adds the two corresponding values from a1 and a2
	for(i = 0; i < sum->Rows(); i++){
		for(j = 0; j < sum->Cols(); j++){
			if(a1->Val(i, j) == '0' && a2->Val(i, j) == '1') sum->Set(i, j, 1);
			else if(a1->Val(i, j) == '1' && a2->Val(i, j) == '0') sum->Set(i, j, 1);
			else sum->Set(i, j, 0);
		}
	}

	return sum;
}

// This function multiplies two matrices together and returns a pointer to product matrix
Bitmatrix *Product(const Bitmatrix *a1, const Bitmatrix *a2) {
	Bitmatrix *product;	// product to be returned
	int i, j, k;		// iterators
	int dot;		// the dot product of a row dot column

	// if matrices can't be multiplied, returns NULL
	if(a1->Cols() != a2->Rows()) return NULL;

	// new matrix is a1's rows by a2's cols
	product = new Bitmatrix(a1->Rows(), a2->Cols());

	// Multiplies the matrices: row dot column for each element
	// iterates through each row in a1
	for(j = 0; j < a1->Rows(); j++){

		// and each column in a2
		for(k = 0; k < a2->Cols(); k++){

			// dot set to 0
			dot = 0;

			// iterates thru each row in a2/ column in a1
			for(i = 0; i < a1->Cols(); i++){
				// and if a1[j][i] and a2[i, k] are both one, each dot is incremented
				if(a1->Val(j, i) == '1' && a2->Val(i, k) == '1') dot += 1;
			}

			// product[j][k] is set to dot mod 2
			dot %= 2;
			product->Set(j, k, dot);

		}
	}

	return product;

}

// This function creates a sub matrix of the rows specified and returns a pointer to it
Bitmatrix *Sub_Matrix(const Bitmatrix *a1, const vector <int> &rows) {
	Bitmatrix *sub;	// sub matrix to be returned
	int i, j;	// iterators

	// returns if matrix is invalid or no rows specified
	if(a1 == NULL) return NULL;
	if(rows.size() <= 0) return NULL;

	// creates new matrix for sub with size of rows number of rows
	sub = new Bitmatrix(rows.size(), a1->Cols());

	// sets the value of each row according to the row from a1
	for(i = 0; (unsigned) i < rows.size(); i++){
		for(j = 0; j < a1->Cols(); j++){
			sub->Set(i, j, a1->Val(rows.at(i), j));
		}
	}

	return sub;
}

// This function finds the inverse a matrix and returns a pointer to the inverse. If matrix is singular, the NULL is returned
Bitmatrix *Inverse(const Bitmatrix *m) {
	Bitmatrix *orig, *inv;
	int i, j;

	if(m->Rows() != m->Cols()) return NULL;

	orig = m->Copy();

	inv = new Bitmatrix(orig->Rows(), orig->Cols());

	for(i = 0; i < inv->Rows(); i++)
		inv->Set(i, i, 1);

	// invert the matrix here

	for(i = 0; i < orig->Rows(); i++){

		// swaps rows such that Row i contains a leading one
		if(orig->Val(i, i) == '0') {

			for(j = i; j < orig->Rows(); j++){

				if(orig->Val(j, i) == '1') {
					orig->Swap_Rows(i, j);
					inv->Swap_Rows(i, j);
					break;
				}
			}

			if(j == orig->Rows()) return NULL;

		}

		// adds rows such to remove all ones below leading one

		for(j = i + 1; j < orig->Rows(); j++){

			if(orig->Val(j, i) == '1') {
				orig->R1_Plus_Equals_R2(j, i);
				inv->R1_Plus_Equals_R2(j, i);
			}

		}

	}

	// now, orig is an upper triangular matrix and inv is lower triangular
	// now, we add rows such to remove all ones above leading one

	for(i -= 1; i >= 0; i--){
		for(j = i - 1; j >= 0; j--){

			if(orig->Val(j, i) == '1'){
				orig->R1_Plus_Equals_R2(j, i);
				inv->R1_Plus_Equals_R2(j, i);
			}
		}
	}

	// now, orig contains the identity and inv contains the inverse

	return inv;
}

// This constructor creates a hash table with specified table size
BM_Hash::BM_Hash(int size) {
	if(size <= 0) throw((string)"Bad size");

	Table.resize(size);
}

// This function stores a bitmatrix into hash table based on key
bool BM_Hash::Store(const string &key, Bitmatrix *bm) {
 	HTE entry;	// The key and bitmatrix to be stored
	size_t i;	// iterator
	unsigned int h;	// hash

	entry.key = key;
	entry.bm = bm;


	// hashes key based on the djb_hash from class
	h = 5381;
	for(i = 0; i < key.size(); i++){
		h += (h << 5) + key[i];
	}

	// now h stores the hash of key, so we can index and store

	// if key is already in vector at the index, returns false
	for(i = 0; i < Table[h % Table.size()].size(); i++){
		if(Table[h % Table.size()].at(i).key == key) return false;
	}

	// otherwise, entry is stored
	Table[h % Table.size()].push_back(entry);

	return true;
}

// This function returns a pointer to a bitmatrix in the hash table
Bitmatrix *BM_Hash::Recall(const string &key) const {
	size_t i;	// iterator
	unsigned int h; // hash

	// hashes key
	h = 5381;
	for(i = 0; i < key.size(); i++){
		h += (h << 5) + key[i];
	}

	// and searches through vector at index to return the pointer of the key
	for(i = 0; i < Table[h % Table.size()].size(); i++){
		if(Table[h % Table.size()].at(i).key == key) return Table[h % Table.size()].at(i).bm;
	}

	// if key wasn't found in table, returns NULL
	return NULL;
}

// This function returns a vector of all the enteries in the hash table
vector <HTE> BM_Hash::All() const {
	vector <HTE> rv;	// vector to return
	size_t i, j;		// iterators

	// iterates through each index in table
	for(i = 0; i < Table.size(); i++){
		if(Table[i].empty()) continue;

		// and adds all the HTE from the vector at index
		for(j = 0; j < Table[i].size(); j++){
			rv.push_back(Table[i].at(j));
		}
	}

	return rv;
}
